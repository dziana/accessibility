class Modal {

    #htmlElements;
    #lastFocusedElement;
    #isModalOpened;

    constructor(modalId, modalContentId, openButtonId, closeButtonId, video) {
        this.#htmlElements = {
            modal: document.getElementById(modalId),
            modalContent: document.getElementById(modalContentId),
            openButton: document.getElementById(openButtonId),
            closeButton: document.getElementById(closeButtonId),
            video: document.getElementById(video),
        };
        this.#isModalOpened = false;
    }

    init() {
        this.#htmlElements.openButton.addEventListener('click', this.#openModal);
        this.#htmlElements.closeButton.addEventListener('click', this.#closeModal);
        this.#htmlElements.modal.addEventListener('click', this.#closeModalByOverlayClick, false);
        document.addEventListener('keydown', this.#closeModal);
        document.addEventListener('keydown', this.#restrictFocus);
    }

    destroy() {
        this.#htmlElements.openButton.removeEventListener('click', this.#openModal);
        this.#htmlElements.closeButton.removeEventListener('click', this.#closeModal);
        this.#htmlElements.modal.removeEventListener('click', this.#closeModalByOverlayClick, false);
        document.removeEventListener('keydown', this.#closeModal);
        document.removeEventListener('keydown', this.#restrictFocus);
    }

    #openModal = () => {
        this.#lastFocusedElement = document.activeElement;
        this.#htmlElements.modal.classList.remove('modal_hidden')
        this.#isModalOpened = true;
        this.#htmlElements.video.focus();
    }

    #closeModal = (event) => {
        if (this.#isModalOpened && (!event.keyCode || event.keyCode === 27)) {
            this.#htmlElements.video.contentWindow.postMessage('{"event":"command","func":"' + 'pauseVideo' + '","args":""}','*');
            this.#htmlElements.modal.classList.add('modal_hidden')
            this.#isModalOpened = false;
            this.#lastFocusedElement.focus();
        }
    }

    #closeModalByOverlayClick = (event) => {
        if (event.target === this.#htmlElements.modalContent.parentNode) {
            this.#closeModal(event);
        }
    }

    #restrictFocus = (event) => {
        if (event.keyCode !== 9) {
            return;
        }

        if (event.shiftKey) {
            if ( this.#isModalOpened && !this.#htmlElements.modal.contains( event.target ) ) {
                this.#htmlElements.closeButton.focus();
                event.preventDefault();
            }
        } else {
            if (document.activeElement === this.#htmlElements.closeButton) {
                this.#htmlElements.video.focus()
                event.preventDefault();
            }
        }
    }
}
