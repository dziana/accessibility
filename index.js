let modal;

window.onload = function () {
    modal = new Modal(
        'modal',
        'modal_content',
        'modal_open_button',
        'modal_close_button',
        'modal_video'
    );
    modal.init();
    lazyLoadImages();
};

window.addEventListener("unload", function () {
    modal.destroy();
});

function lazyLoadImages() {
    const lazyBackgrounds =
        Array.from(document.querySelectorAll(
            ".page_image-lazy, .play-button_image-lazy, .close-button_image-lazy"
        ));

    if ("IntersectionObserver" in window) {
        const lazyBackgroundObserver = new IntersectionObserver(function (entries) {
            entries.forEach(function (entry) {
                if (entry.isIntersecting) {
                    const className = Array.from(entry.target.classList).find((e) => e.includes('image-lazy'));
                    entry.target.classList.remove(className);
                    lazyBackgroundObserver.unobserve(entry.target);
                }
            });
        });

        lazyBackgrounds.forEach(function (lazyBackground) {
            lazyBackgroundObserver.observe(lazyBackground);
        });
    }
}
